### Print function

```python
print("My name is", "Python.", end=" ")
print("Monty Python.")
```

Will read as :

My name is Python. Monty Python.

Note that the comma between the two arguments does not affect any spacing and the 'end' (the third argument) causes the two lines of code to be on the same line in the output. You can mess around with the spacing and have the same outcome.

```python
print("My name is ", end="")
print("Monty Python.")
```


In the previous example, `end` was the keyword. In the following, `sep` is the keyword:

```python
print("My", "name", "is", "Monty", "Python.", sep="-")
```
Giving the outcome 'My-name-is-Monty-Python.'. The - between speech marks in the `sep` part can be replaced with a space and will have the same effect as when `end` was used.

Having an empty print function, `print()`, will print a blank line. 



### Random note

Data and operators when connected together form expressions. The simplest expression is a literal itself.



### Operators

When carrying out mathematical things in Python, if any argument is a float, th result will be a float. An exception to this rule is with division where, even if all arguments are integers, the result will be a float. Division is carried out using `/`  but if you want an integer result, you can use `//` but all arguments must be integers. However, using this method (called integer division or floor division), means that your result will be rounded down.

This is also the case for negative integers. Input:
```python
print(-6 // 4)
print(6. // -4)
```

Output:
```python
-2
-2.0
```

Division rules, do not:
- perform a division by zero;
- perform an integer division by zero;
- find a remainder of a division by zero.

The remainder operator (%) can also be called modulo.
14 // 4 gives 3 → this is the integer quotient;
3 * 4 gives 12 → as a result of quotient and divisor multiplication;
14 - 12 gives 2 → this is the remainder.

Names you can't use for variables or functions:
['False', 'None', 'True', 'and', 'as', 'assert', 'break', 'class', 'continue', 'def', 'del', 'elif', 'else', 'except', 'finally', 'for', 'from', 'global', 'if', 'import', 'in', 'is', 'lambda', 'nonlocal', 'not', 'or', 'pass', 'raise', 'return', 'try', 'while', 'with', 'yield']


If op is a two-argument operator (this is a very important condition) and the operator is used in the following context:

variable = variable op expression

E.g.: variable = variable op expression --> i += 2 * j


`round(miles_to_kilometers, 2)` will round the variable to 2 decimal places.


```python
a = '1'
b = "1"
print(a + b)
```

Output would be 11.


A program which doesn't get a user's input is a deaf program.


Number having 0o or 0O as prefix represents octal number which can have 0 to 7 as one of the digits in it. Example: 0O12: equivalent to 10 (ten) in decimal number system. It works in the same way as binary or base 10: first digit (working right to left) is one, the next is eight and the one after that would be 64. So each time you move to the left it goes up by a power of eight.



### Example using `while`, `break` and `continue`


```python
secret = input("Pick a word any word or say 'cool' to end the game: ")

while secret != "cool" :
    secret = input("Pick a word any word or say 'cool' to end the game: ")
    continue
    if secret == "cool":
        break
print("Game over :/")
```

Notice that everything is inside the while loop. Except for the print function which I tested and would not actually print if it was within the loop.


`for` and `while` loops can also have `else` parts to them but they are not within the loop.

Okay I'm saving this because I'm quite proud of how quickly I got it but more because I got it even though I didn't get it if that makes sense? Like I'd typed it out as if I just knew and then worked through it after. Does that make sense? Like usually I would think about it, make sure that it works and then type it out but this was just trusting my brain and it was right! I feel like this is what's lying underneath all this cloud of low confidence. I can't explain it 😩

But anyway, here's the question:

A boy and his father, a computer programmer, are playing with wooden blocks. They are building a pyramid. Their pyramid is a bit weird, as it is actually a pyramid-shaped wall - it's flat. The pyramid is stacked according to one simple principle: each lower layer contains one block more than the layer above. Your task is to write a program which reads the number of blocks the builders have, and outputs the height of the pyramid that can be built using these blocks. Note: the height is measured by the number of fully completed layers - if the builders don't have a sufficient number of blocks and cannot complete the next layer, they finish their work immediately. 

```python
blocks = int(input("Enter the number of blocks: "))
height = 0

while blocks > height:
    height += 1
    blocks -= height

print("The height of the pyramid:", height)
```

It's cute, it's concise, it does the job! It took less time than expected (literally a couple of minutes to get the basics out then, because it didn't work for all the test data, the most part was being the number and working through it to fix it), I love it. I'm gonna be honest I don't know at what point in time it started working because I could've sworn that what's there was already there when it was both working and not working so I'm a lil confused. But the point is it's there, woo!

Another question: 

In 1937, a German mathematician named Lothar Collatz formulated an intriguing hypothesis (it still remains unproven) which can be described in the following way:

1. take any non-negative and non-zero integer number and name it c0;
2. if it's even, evaluate a new c0 as c0 ÷ 2;
3. otherwise, if it's odd, evaluate a new c0 as 3 × c0 + 1;
4. if c0 ≠ 1, skip to point 2.
   
The hypothesis says that regardless of the initial value of c0, it will always go to 1.

My code:
```python
c0 = int(input("Give me a number: "))
steps = 0
while c0 > 0 and c0 != 1:
    if c0 % 2 == 0:
        c0 = c0//2
        print(c0)
        steps += 1
    else:
        c0 = 3 * c0 + 1
        print(c0)
        steps += 1
        
print("Steps = ", steps)
```


One logical conjunction operator in Python is the word `and`. It's a binary operator with a priority that is lower than the one expressed by the comparison operators.

A disjunction operator is the word `or`. It's a binary operator with a lower priority than `and` (just like + compared to *).

### Truth tables

For overall true and false, a pair which is different (true, false or false, true) gives false for `and` and true for `or`. For both `and` and `or`, if they're both the same, the overall is the same as the two components.

You may be familiar with De Morgan's laws. They say that:

The negation of a conjunction is the disjunction of the negations.

The negation of a disjunction is the conjunction of the negations.

In python:
```python
not (p and q) == (not p) or (not q)
not (p or q) == (not p) and (not q)
```

Zero = false, not zero = true.

There are four bitwise operators:

& (ampersand) - bitwise conjunction;

| (bar) - bitwise disjunction;

~ (tilde) - bitwise negation;

^ (caret) - bitwise exclusive or (xor). Exclusive or or exclusive disjunction is a logical operation that is true if and only if its arguments differ.

Arguments of these operators must be integers, not floats.
The ampersand works in the same way as `and` but operates on individual bits, bit by bit.


Bitwise negation:

```
i -         00000000000000000000000000001111
bitneg = ~i	11111111111111111111111111110000
```

### Bitwise operators

x = 15, which is 0000 1111 in binary,
y = 16, which is 0001 0000 in binary.

```
& does a bitwise and, e.g., x & y = 0, which is 0000 0000 in binary,
| does a bitwise or, e.g., x | y = 31, which is 0001 1111 in binary,
˜  does a bitwise not, e.g., ˜ x = 240*, which is 1111 0000 in binary,
^ does a bitwise xor, e.g., x ^ y = 31, which is 0001 1111 in binary,
>> does a bitwise right shift, e.g., y >> 1 = 8, which is 0000 1000 in binary,
<< does a bitwise left shift, e.g., y << 3 = , which is 1000 0000 in binary,
```



Let's assume that the following assignments have been performed:

```
i = 15
j = 22
```

If we assume that the integers are stored with 32 bits, the bitwise image of the two variables will be as follows:

```
i: 00000000000000000000000000001111
j: 00000000000000000000000000010110
```

The assignment is given:
`log = i and j`

We are dealing with a logical conjunction here. Let's trace the course of the calculations. Both variables i and j are not zeros, so will be deemed to represent True. Consulting the truth table for the and operator, we can see that the result will be True. No other operations are performed.

`log: True`

Now the bitwise operation:

`bit = i & j`

The & operator will operate with each pair of corresponding bits separately, producing the values of the relevant bits of the result. Therefore, the result will be as follows:

```

i  =	    00000000000000000000000000001111
j	=       00000000000000000000000000010110
bit = i & j	00000000000000000000000000000110
```

So working from right to left here, 1 & 0 are different and so the overall is false i.e. 0. Same rules for the rest. These bits correspond to the integer value of six.

Imagine that you're a developer obliged to write an important piece of an operating system. You've been told that you're allowed to use a variable assigned in the following way:

```
flag_register = 0x1234
```

The variable stores the information about various aspects of system operation. Each bit of the variable stores one yes/no value. You've also been told that only one of these bits is yours - the third (remember that bits are numbered from zero, and bit number zero is the lowest one, while the highest is number 31. Also remember that the count starts from the right). The remaining bits are not allowed to change, because they're intended to store other data. Here's your bit marked with the letter x:

```
flag_register = 0000000000000000000000000000x000
```

You may be asked to carry out the following tasks:

1. Check the state of your bit - you want to find out the value of your bit; comparing the whole variable to zero will not do anything, because the remaining bits can have completely unpredictable values, but you can use the following conjunction property:

```
x & 1 = x
x & 0 = 0
```

If you apply the & operation to the flag_register variable along with the following bit image:

`00000000000000000000000000001000`

(note the 1 at your bit's position) as the result, you obtain one of the following bit strings:

`00000000000000000000000000001000` if your bit was set to 1

`0000000000000000000000000000000` if your bit was reset to 0

Such a sequence of zeros and ones, whose task is to grab the value or to change the selected bits, is called a bit mask.

Let's build a bit mask to detect the state of your bit. It should point to the third bit. That bit has the weight of 23 = 8. A suitable mask could be created by the following declaration:
`the_mask = 8`

You can also make a sequence of instructions depending on the state of your bit i, here it is:
```
if flag_register & the_mask:
    # My bit is set.
else:
    # My bit is reset.
```

2. Reset your bit - you assign a zero to the bit while all the other bits must remain unchanged; let's use the same property of the conjunction as before, but let's use a slightly different mask - exactly as below:
`11111111111111111111111111110111`

Note that the mask was created as a result of the negation of all the bits of the_mask variable. Resetting the bit is simple, and looks like this (choose the one you like more):

```
flag_register = flag_register & ~the_mask
flag_register &= ~the_mask
```

3. Set your bit - you assign a 1 to your bit, while all the remaining bits must remain unchanged; use the following disjunction property:

```
x | 1 = 1
x | 0 = x
```

You're now ready to set your bit with one of the following instructions:

```
flag_register = flag_register | the_mask
flag_register |= the_mask
```

4. Negate your bit - you replace a 1 with a 0 and a 0 with a 1. You can use an interesting property of the xor operator:

```
x ^ 1 = ~x
x ^ 0 = x
```

and negate your bit with the following instructions:

```
flag_register = flag_register ^ the_mask
flag_register ^= the_mask
```

### Shifting

In Python, shifting is like multiplying or dividing by ten. But because we're working in binary, base two, it's the equivalent of multiplying (shift digits to the left) or dividing by two (right). You can use `<<` and `>>` to shift.

E.g:
```
var = 17
var_right = var >> 1
var_left = var << 2
print(var, var_left, var_right)
```

Will give an output of '17 68 8'.


Due to the syntax of this operation, it is not commutative. This means that you can't sswitch the order around. In addition to this, the priority of this operation is very high.


### Lists

A list is a collection of elements, but each element is a scalar.
The first element is always at position zero.

```
numbers[0] = 111
print("New list content: ", numbers)  # Current list content.
```

The above will replace whatever was in the first position of the list names 'numbers' with 111, it doens't push everything down one and put 111 at the front.

You can also do this: `numbers[1] = numbers[4]`.

The value inside the brackets which selects one element of the list is called an index, while the operation of selecting an element from the list is known as indexing.

Note: all the indices used so far are literals. Their values are fixed at runtime, but any expression can be the index, too. 

You can print specific elements of your list using something like `print(numbers[0])`.

To check the length of a list, you can use `len(numbers)`. This function takes the name of the list as its argument and returns the number of elements stored within.

`del` is an instruction, not a function and can be used to remove elements from a list: `del numbers[1]`

An element with an index equal to -1 is the last one in the list. You can continue in this way with -2, -3 etc as long as there are enough elements in the list.


### Functions vs. methods

A method is a specific kind of function - it behaves like a function and looks like a function, but differs in the way in which it acts, and in its invocation style.

A method does the same things as a function (gets data, it may create new data and it (generally) produces a result) as well as change the state of a selected entity. A method is owned by the data it works for, while a function is owned by the whole code.

A method is owned by the data it works for, while a function is owned by the whole code. 

In general, a typical function invocation may look like this::
`result = function(arg)`

A typical method invocation usually looks like this:
`result = data.method(arg)`

If you want to add onto the end of a list, you can do `<nameoflist>.append(whateveryouwantotaddon)` which owns the method. The length of the list will then increase by one.

To insert a value anywhere else in the list, you can use `list.insert(position, value)`.

This:
```
for i in range(5)
    my_list.insert(0, i + 1)
```

Gives this:

`[5, 4, 3, 2, 1]`

This is a method you can use to add up all elements in a list:
```
my_list = [10, 1, 8, 3, 5]
total = 0

for i in range(len(my_list)):
    total += my_list[i]
```

A somewhat simplified version:
```
my_list = [10, 1, 8, 3, 5]
total = 0

for i in my_list:
    total += i

print(total)
```

If you wanna swap the order of something:
```
variable_1 = 1
variable_2 = 2

variable_1, variable_2 = variable_2, variable_1
```

The list is a type of data in Python used to store multiple objects. It is an ordered and mutable collection of comma-separated items between square brackets.

So for swapping elements in a list:
```
my_list = [10, 1, 8, 3, 5]

my_list[0], my_list[4] = my_list[4], my_list[0]
my_list[1], my_list[3] = my_list[3], my_list[1]
```

For a list with something like 100 elements though... sheesh. Good job we have the trusty old for loop!
```
my_list = [10, 1, 8, 3, 5]
length = len(my_list)

for i in range(length // 2):
    my_list[i], my_list[length - i - 1] = my_list[length - i - 1], my_list[i]
```

Dividing the length by 2 works for lists with both even and odd lengths, because when the list contains an odd number of elements, the middle one remains untouched.


Also:
```bash
my_list = [1, None, True, 'I am a string', 256, 0]

my_list[1] = '?'
print(my_list)  # outputs: [1, '?', True, 'I am a string', 256, 0]
```

